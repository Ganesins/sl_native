export default ({ config }) => {
  return {
    ...config,
    // Other dynamic configuration options
    extra: {
      eas: {
        projectId: "a503c42d-8790-41e8-9b0d-e394058de191",
      },
    },
    plugins: [
      [
        "expo-build-properties",
        {
          android: {
            enableProguardInReleaseBuilds: true,
            enableShrinkResourcesInReleaseBuilds: true,
          },
        },
      ],
    ],
  };
};
