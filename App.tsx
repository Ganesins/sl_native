import { useEffect, useRef, useState } from "react";
import { Platform, BackHandler, Alert } from "react-native";
import { WebView, WebViewMessageEvent } from "react-native-webview";
import * as SplashScreen from "expo-splash-screen";
import * as Location from 'expo-location';
import { WebviewWrapper } from "./src/components/WebviewWrapper";
import { Camera } from "expo-camera";

SplashScreen.preventAutoHideAsync();

const BASE_URL = "https://app-staffbillingportal.samudhrikalakshana.co.in/";

export default function App() {
  const webViewRef = useRef<WebView>(null);
  const [loading, setLoading] = useState<boolean>(true);

  // Handle hiding the splash screen
  useEffect(() => {
    if (loading) return;
    // Wait a bit before hiding the splash screen to avoid flickering
    const timer = setTimeout(async () => {
      await SplashScreen.hideAsync();
    }, 200);

    return () => clearTimeout(timer);
  }, [loading]);

  // Handle Android back button
  useEffect(() => {
    if (Platform.OS !== "android") return;
    const handleBack = () => {
      if (!webViewRef.current) return false;
      webViewRef.current.goBack();
      return true;
    };

    const handleEvent = BackHandler.addEventListener(
      "hardwareBackPress",
      handleBack
    );
    return () => handleEvent.remove();
  }, []);

  // Check permissions
  useEffect(() => {
    checkLocationPermission();
    checkCameraPermission();
  }, []);

  // Check location permission
  const checkLocationPermission = async () => {
    try {
      const { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        Alert.alert(
          'Permission denied',
          'You must enable location permissions to use this app.'
        );
      }
    } catch (error) {
      console.log('Error requesting location permission:', error);
    }
  };

  // Check CameraPermission
  const checkCameraPermission = async () => {
    try {
      let { status } = await Camera.requestCameraPermissionsAsync();
      if (status !== 'granted') {
        Alert.alert(
          'Permission denied',
          'You must enable camera permissions to use this app.'
        );
      }
    } catch (error) {
      console.log('Error requesting camera permission:', error);
    }
  };

  // Handle the message as needed
  const handleMessage = (event: WebViewMessageEvent) => {
    const { data } = event.nativeEvent;
    console.log('Message received from webview:', data);
    if (data === 'getLocation') {
      fetchLocation();
    } else if (data === 'checkCameraPermission') {
      checkCameraPermission();
    }
  };

  // dispatch message event
  const generateOnMessageFunction = (data: any) =>
    `(function() {
      window.dispatchEvent(new MessageEvent('message', {data: ${JSON.stringify(data)}}));
    })()`;

  // Fetch Geolocation
  const fetchLocation = async () => {
    try {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        throw new Error('Location permission not granted');
      }

      const position = await Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.BestForNavigation });
      // Send location data to the parent component
      webViewRef?.current?.injectJavaScript(generateOnMessageFunction(position.coords));
      console.log('Message sent to webview:', JSON.stringify(position.coords));
    } catch (error) {
      console.error('Error fetching location: ', error);
    }
  };

  /*
   assign function to postmessage
   window.ReactNativeWebView.postMessage will give event to onMessage
  */
  const injectedJavascript = `(function() {
    window.postMessage = function(data) {
      console.log('Redirect message to webview:', data);
      window.ReactNativeWebView.postMessage(data);
    };
  })()`;

  return (
    <WebviewWrapper>
      <WebView
        ref={webViewRef}
        source={{
          uri: BASE_URL,
        }}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        injectedJavaScript={injectedJavascript}
        allowsBackForwardNavigationGestures
        userAgent={`webview-${Platform.OS}`}
        onLoadEnd={() => setLoading(false)}
        onMessage={handleMessage}
      />
    </WebviewWrapper>
  );
}